# AMCL

Another Minecraft Launcher is a Minecraft: Java Edition launcher written in C# that is inspired by the
vanilla launcher and several other MultiMC forks and Chinese launchers.

## Features

- [ ] Launch Minecraft: Java Edition
- [ ] Microsoft Account (MSA) Authentication
- [x] Automatic assets, libraries, natives management
- [x] Assets and libraries mirrors support
- [x] Versions and instances
- [ ] Automatic Java Runtime management
- [ ] Automatic mods download and install support
- [ ] Automatic resource pack download and install support

## Usage

The program is in early development and is very not suitable for usage.

## Licence

This program (project) is licensed under the terms of the
GNU General Public License, either version 3 of the License, or
(at your option) any later version.

The full copyright notice for this project, and the copyright notices of all
other works used by this project can be found in the [CREDITS](CREDITS) file.

The full text of the licence of this project can be found in the
[COPYING](COPYING) file.