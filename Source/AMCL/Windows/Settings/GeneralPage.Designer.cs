﻿namespace AMCL.Windows.Settings;

partial class GeneralPage
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        labelMirror = new Label();
        comboMirror = new ComboBox();
        noticeBmclApi = new Controls.NoticeBar();
        labelAbout = new Label();
        labelLauncher = new Label();
        labelFrontend = new Label();
        labelCopyright = new Label();
        labelCredits = new LinkLabel();
        SuspendLayout();
        // 
        // labelMirror
        // 
        labelMirror.Location = new Point(12, 11);
        labelMirror.Name = "labelMirror";
        labelMirror.Size = new Size(66, 27);
        labelMirror.TabIndex = 0;
        labelMirror.Text = "Mirror";
        labelMirror.TextAlign = ContentAlignment.MiddleLeft;
        // 
        // comboMirror
        // 
        comboMirror.BackColor = Color.FromArgb(43, 43, 43);
        comboMirror.DropDownStyle = ComboBoxStyle.DropDownList;
        comboMirror.FormattingEnabled = true;
        comboMirror.Items.AddRange(new object[] { "Mojang (Default)", "BMCLAPI" });
        comboMirror.Location = new Point(67, 11);
        comboMirror.Name = "comboMirror";
        comboMirror.Size = new Size(172, 28);
        comboMirror.TabIndex = 1;
        // 
        // noticeBmclApi
        // 
        noticeBmclApi.BackColor = Color.FromArgb(255, 128, 0);
        noticeBmclApi.Description = "BMCLAPI is operated by bangbang93. For more information, visit: http://bmclapidoc.bangbang93.com/";
        noticeBmclApi.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        noticeBmclApi.Location = new Point(12, 45);
        noticeBmclApi.MinimumSize = new Size(269, 96);
        noticeBmclApi.Name = "noticeBmclApi";
        noticeBmclApi.Size = new Size(525, 96);
        noticeBmclApi.TabIndex = 2;
        noticeBmclApi.Title = "BMCLAPI";
        // 
        // labelAbout
        // 
        labelAbout.AutoSize = true;
        labelAbout.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
        labelAbout.Location = new Point(12, 175);
        labelAbout.Name = "labelAbout";
        labelAbout.Size = new Size(53, 20);
        labelAbout.TabIndex = 3;
        labelAbout.Text = "About";
        // 
        // labelLauncher
        // 
        labelLauncher.AutoSize = true;
        labelLauncher.Location = new Point(12, 195);
        labelLauncher.Name = "labelLauncher";
        labelLauncher.Size = new Size(99, 20);
        labelLauncher.TabIndex = 4;
        labelLauncher.Text = "AMCL version";
        // 
        // labelFrontend
        // 
        labelFrontend.AutoSize = true;
        labelFrontend.Location = new Point(12, 215);
        labelFrontend.Name = "labelFrontend";
        labelFrontend.Size = new Size(68, 20);
        labelFrontend.TabIndex = 5;
        labelFrontend.Text = "Frontend";
        // 
        // labelCopyright
        // 
        labelCopyright.AutoSize = true;
        labelCopyright.Location = new Point(12, 249);
        labelCopyright.Name = "labelCopyright";
        labelCopyright.Size = new Size(309, 20);
        labelCopyright.TabIndex = 6;
        labelCopyright.Text = "Copyright (C) NexusKrop && contributors 2023";
        // 
        // labelCredits
        // 
        labelCredits.AutoSize = true;
        labelCredits.LinkColor = Color.DodgerBlue;
        labelCredits.Location = new Point(12, 269);
        labelCredits.Name = "labelCredits";
        labelCredits.Size = new Size(143, 20);
        labelCredits.TabIndex = 7;
        labelCredits.TabStop = true;
        labelCredits.Text = "Licences and Credits";
        labelCredits.VisitedLinkColor = Color.FromArgb(192, 192, 255);
        labelCredits.LinkClicked += labelCredits_LinkClicked;
        // 
        // GeneralPage
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(43, 43, 43);
        Controls.Add(labelCredits);
        Controls.Add(labelCopyright);
        Controls.Add(labelFrontend);
        Controls.Add(labelLauncher);
        Controls.Add(labelAbout);
        Controls.Add(noticeBmclApi);
        Controls.Add(comboMirror);
        Controls.Add(labelMirror);
        Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        ForeColor = Color.White;
        Name = "GeneralPage";
        Size = new Size(559, 359);
        Load += GeneralPage_Load;
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Label labelMirror;
    private ComboBox comboMirror;
    private Controls.NoticeBar noticeBmclApi;
    private Label labelAbout;
    private Label labelLauncher;
    private Label labelFrontend;
    private Label labelCopyright;
    private LinkLabel labelCredits;
}
