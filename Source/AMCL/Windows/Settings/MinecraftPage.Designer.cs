﻿namespace AMCL.Windows.Settings;

partial class MinecraftPage
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        labelRepository = new Label();
        textRepository = new TextBox();
        buttonFind = new Button();
        buttonReset = new Button();
        SuspendLayout();
        // 
        // labelRepository
        // 
        labelRepository.AutoSize = true;
        labelRepository.Location = new Point(11, 11);
        labelRepository.Name = "labelRepository";
        labelRepository.Size = new Size(141, 20);
        labelRepository.TabIndex = 0;
        labelRepository.Text = "Repository Location";
        // 
        // textRepository
        // 
        textRepository.BackColor = Color.FromArgb(45, 45, 45);
        textRepository.BorderStyle = BorderStyle.FixedSingle;
        textRepository.Location = new Point(11, 44);
        textRepository.Name = "textRepository";
        textRepository.ReadOnly = true;
        textRepository.Size = new Size(414, 27);
        textRepository.TabIndex = 1;
        // 
        // buttonFind
        // 
        buttonFind.BackColor = Color.FromArgb(64, 64, 64);
        buttonFind.FlatAppearance.BorderColor = Color.Black;
        buttonFind.FlatAppearance.MouseDownBackColor = Color.Black;
        buttonFind.FlatAppearance.MouseOverBackColor = Color.Gray;
        buttonFind.FlatStyle = FlatStyle.Flat;
        buttonFind.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        buttonFind.ForeColor = Color.White;
        buttonFind.Location = new Point(430, 44);
        buttonFind.Name = "buttonFind";
        buttonFind.Size = new Size(36, 27);
        buttonFind.TabIndex = 4;
        buttonFind.Text = "...";
        buttonFind.UseVisualStyleBackColor = false;
        buttonFind.Click += buttonFind_Click;
        // 
        // buttonReset
        // 
        buttonReset.BackColor = Color.FromArgb(64, 64, 64);
        buttonReset.FlatAppearance.BorderColor = Color.Black;
        buttonReset.FlatAppearance.MouseDownBackColor = Color.Black;
        buttonReset.FlatAppearance.MouseOverBackColor = Color.Gray;
        buttonReset.FlatStyle = FlatStyle.Flat;
        buttonReset.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        buttonReset.ForeColor = Color.White;
        buttonReset.Location = new Point(11, 87);
        buttonReset.Name = "buttonReset";
        buttonReset.Size = new Size(187, 30);
        buttonReset.TabIndex = 5;
        buttonReset.Text = "Reset Repository Location";
        buttonReset.UseVisualStyleBackColor = false;
        buttonReset.Click += buttonReset_Click;
        // 
        // MinecraftPage
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(45, 45, 45);
        Controls.Add(buttonReset);
        Controls.Add(buttonFind);
        Controls.Add(textRepository);
        Controls.Add(labelRepository);
        Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        ForeColor = Color.White;
        Name = "MinecraftPage";
        Size = new Size(481, 425);
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Label labelRepository;
    private TextBox textRepository;
    private Button buttonFind;
    private Button buttonReset;
}
