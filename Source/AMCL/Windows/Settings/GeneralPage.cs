﻿namespace AMCL.Windows.Settings;

using NexusKrop.IceCube;
using System;
using System.Windows.Forms;
using AMCL.Core.Launch;

public partial class GeneralPage : UserControl, ISettingsPage
{
    public GeneralPage()
    {
        InitializeComponent();
    }

    public void Save()
    {
        Settings.Default.Mirror = comboMirror.SelectedIndex;
    }

    private void GeneralPage_Load(object sender, EventArgs e)
    {
        comboMirror.SelectedIndex = Settings.Default.Mirror;

        labelLauncher.Text = $"{Launcher.Name} version {Launcher.Version}";
        labelFrontend.Text = $"Frontend: {Application.ProductVersion}";

        AmclApp.SetLicences();
    }

    private void labelCredits_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
        if (AmclApp.LicencesOk)
        {
            ProcessUtil.ShellExecute(AmclApp.LicencesPath);
        }
        else
        {
            MessageBox.Show("Credits file not available. Please ensure that the Launcher has access to the folder that it is in.");
        }
    }
}
