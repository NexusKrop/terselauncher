﻿namespace AMCL.Windows.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AMCL.Properties;

public partial class MinecraftPage : UserControl, ISettingsPage
{
    public MinecraftPage()
    {
        InitializeComponent();
    }

    public void Save()
    {
        Settings.Default.RepositoryLocation = textRepository.Text;
    }

    private void buttonFind_Click(object sender, EventArgs e)
    {
        var fbd = new FolderBrowserDialog()
        {
            ShowNewFolderButton = true,
        };
        if (fbd.ShowDialog() == DialogResult.OK)
        {
            textRepository.Text = fbd.SelectedPath;
        }
    }

    private void buttonReset_Click(object sender, EventArgs e)
    {
        textRepository.Clear();
    }
}
