﻿namespace AMCL.Windows.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public abstract class SettingsPage : UserControl
{
    public abstract void Save();
}
