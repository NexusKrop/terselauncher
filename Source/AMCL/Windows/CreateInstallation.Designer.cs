﻿namespace AMCL.Windows;

partial class CreateInstallation
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        labelName = new Label();
        textName = new TextBox();
        buttonCreate = new Button();
        buttonCancel = new Button();
        panelResult = new Panel();
        labelVersion = new Label();
        progressLoad = new ProgressBar();
        listVersions = new ComboBox();
        panelResult.SuspendLayout();
        SuspendLayout();
        // 
        // labelName
        // 
        labelName.AutoSize = true;
        labelName.Location = new Point(12, 14);
        labelName.Name = "labelName";
        labelName.Size = new Size(52, 20);
        labelName.TabIndex = 0;
        labelName.Text = "Name";
        // 
        // textName
        // 
        textName.BackColor = Color.FromArgb(43, 43, 43);
        textName.BorderStyle = BorderStyle.FixedSingle;
        textName.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        textName.ForeColor = Color.White;
        textName.Location = new Point(70, 12);
        textName.Name = "textName";
        textName.Size = new Size(718, 27);
        textName.TabIndex = 1;
        // 
        // buttonCreate
        // 
        buttonCreate.BackColor = Color.ForestGreen;
        buttonCreate.Enabled = false;
        buttonCreate.FlatAppearance.BorderColor = Color.FromArgb(0, 64, 0);
        buttonCreate.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 64, 0);
        buttonCreate.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 192, 0);
        buttonCreate.FlatStyle = FlatStyle.Flat;
        buttonCreate.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
        buttonCreate.ForeColor = Color.White;
        buttonCreate.Location = new Point(713, 11);
        buttonCreate.Name = "buttonCreate";
        buttonCreate.Size = new Size(75, 31);
        buttonCreate.TabIndex = 2;
        buttonCreate.Text = "Create";
        buttonCreate.UseVisualStyleBackColor = false;
        buttonCreate.Click += buttonCreate_Click;
        // 
        // buttonCancel
        // 
        buttonCancel.BackColor = Color.FromArgb(64, 64, 64);
        buttonCancel.FlatAppearance.BorderColor = Color.Black;
        buttonCancel.FlatAppearance.MouseDownBackColor = Color.Black;
        buttonCancel.FlatAppearance.MouseOverBackColor = Color.Gray;
        buttonCancel.FlatStyle = FlatStyle.Flat;
        buttonCancel.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        buttonCancel.ForeColor = Color.White;
        buttonCancel.Location = new Point(632, 11);
        buttonCancel.Name = "buttonCancel";
        buttonCancel.Size = new Size(75, 31);
        buttonCancel.TabIndex = 3;
        buttonCancel.Text = "Cancel";
        buttonCancel.UseVisualStyleBackColor = false;
        buttonCancel.Click += buttonCancel_Click;
        // 
        // panelResult
        // 
        panelResult.BackColor = Color.FromArgb(43, 43, 43);
        panelResult.Controls.Add(buttonCreate);
        panelResult.Controls.Add(buttonCancel);
        panelResult.Location = new Point(0, 396);
        panelResult.Name = "panelResult";
        panelResult.Size = new Size(802, 55);
        panelResult.TabIndex = 4;
        // 
        // labelVersion
        // 
        labelVersion.AutoSize = true;
        labelVersion.Location = new Point(12, 48);
        labelVersion.Name = "labelVersion";
        labelVersion.Size = new Size(64, 20);
        labelVersion.TabIndex = 5;
        labelVersion.Text = "Version";
        // 
        // progressLoad
        // 
        progressLoad.Location = new Point(0, 387);
        progressLoad.Name = "progressLoad";
        progressLoad.Size = new Size(799, 14);
        progressLoad.Style = ProgressBarStyle.Marquee;
        progressLoad.TabIndex = 7;
        // 
        // listVersions
        // 
        listVersions.DropDownStyle = ComboBoxStyle.DropDownList;
        listVersions.FormattingEnabled = true;
        listVersions.Location = new Point(82, 45);
        listVersions.Name = "listVersions";
        listVersions.Size = new Size(183, 28);
        listVersions.TabIndex = 8;
        // 
        // CreateInstallation
        // 
        AutoScaleDimensions = new SizeF(9F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(35, 35, 35);
        ClientSize = new Size(800, 450);
        Controls.Add(listVersions);
        Controls.Add(progressLoad);
        Controls.Add(labelVersion);
        Controls.Add(panelResult);
        Controls.Add(textName);
        Controls.Add(labelName);
        ForeColor = Color.White;
        FormBorderStyle = FormBorderStyle.FixedDialog;
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "CreateInstallation";
        Text = "Create New Installation";
        Load += CreateInstallation_Load;
        panelResult.ResumeLayout(false);
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Label labelName;
    private TextBox textName;
    private Button buttonCreate;
    private Button buttonCancel;
    private Panel panelResult;
    private Label labelVersion;
    private ProgressBar progressLoad;
    private ComboBox listVersions;
}