﻿namespace AMCL.Windows;

using DarkUI.Controls;
using Newtonsoft.Json;
using NexusKrop.IceCube;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AMCL.Core.Launch;
using AMCL.Core.Resources.Game.Client;
using AMCL.Core.Resources.Game.Versions;
using AMCL.Core.Tasks;
using AMCL.Repository;
using AMCL.Windows.Dialogs;

public partial class CreateInstallation : Form
{
    private ClientVersionManifest? _manifest;
    private Exception? _failure;
    private List<ClientMetadataPointer> _items = new();

    private readonly BackgroundWorker _worker = new();
    private bool cancelWork = false;

    public CreateInstallation()
    {
        InitializeComponent();

        _worker.DoWork += _worker_DoWork;
        _worker.WorkerReportsProgress = true;
        _worker.ProgressChanged += _worker_ProgressChanged;
    }

    private void _worker_ProgressChanged(object? sender, ProgressChangedEventArgs e)
    {
        if (cancelWork)
        {
            return;
        }

        if (_failure != null)
        {
            this.Invoke(() =>
            {
                MessageBox.Show(_failure.ToString());
                Close();
            });

            return;
        }

        progressLoad.Style = ProgressBarStyle.Marquee;

        foreach (var item in _items)
        {
            if (cancelWork)
            {
                return;
            }
            listVersions.Items.Add(item);
        }

        listVersions.SelectedIndex = 0;

        Debug.Assert(_manifest != null);

        this.Invoke(() =>
        {
            buttonCreate.Enabled = true;
            listVersions.Enabled = true;
            progressLoad.Visible = false;
        });
    }

    private void _worker_DoWork(object? sender, DoWorkEventArgs e)
    {
        try
        {
            using (var reader = new JsonTextReader(new StreamReader(Launcher.HttpClient.GetStreamAsync(AmclApp.Mirror.VersionManifest).GetAwaiter().GetResult())))
            {
                _manifest = Launcher.Serializer.Deserialize<ClientVersionManifest>(reader);
            }

            Invoke(() => progressLoad.Style = ProgressBarStyle.Continuous);
            var counter = 0;
            var xc = 0;
            var total = _manifest!.Versions.Count;

            foreach (var version in _manifest!.Versions)
            {
                if (cancelWork)
                {
                    return;
                }

                counter++;
                xc++;

                _items.Add(version);

                Invoke(() =>
                {
                    progressLoad.Value = MathUtil.CalculatePercentage(counter, total);
                });

                if (cancelWork)
                {
                    return;
                }

                if (xc >= 5)
                {
                    xc = 0;
                    BeginInvoke(() => progressLoad.Refresh());
                }
            }

            if (_items.IsEmpty())
            {
                Invoke(() =>
                {
                    MessageBox.Show("No versions available");
                    Close();
                });
                return;
            }

            _worker.ReportProgress(100);
        }
        catch (Exception ex)
        {
            _failure = ex;
            _worker.ReportProgress(100);
        }
    }

    private void CreateInstallation_Load(object sender, EventArgs e)
    {
        _worker.RunWorkerAsync();
    }

    private void buttonCreate_Click(object sender, EventArgs e)
    {
        var ver = (ClientMetadataPointer)listVersions.SelectedItem;

        var progress = new ProgressDialog(async obj => await PerformCreate(textName.Text, obj, ver));
        this.Enabled = false;
        progress.ShowDialog();

        this.Close();
    }

    private async Task PerformCreate(string name, ProgressDialog obj, ClientMetadataPointer ver)
    {
        obj.UpdateMessage("Restoring client");

        var versionPath = Path.Combine(AmclApp.StorageConfig.Repository.VersionPath, ver.Id);
        var jsonPath = Path.Combine(versionPath, $"{ver.Id}.json");

        AmclApp.CreateDirectorySafe(versionPath);

        if (!await Launcher.IsFileIntact(versionPath, ver.Sha1))
        {
            await Launcher.DownloadFileAsync(ver.Url, jsonPath, 0, false,
                obj.UpdateProgress);
        }

        obj.UpdateProgress(0);

        ClientMetadata? metadata;

        // Read client.json
        using (var reader = Launcher.OpenReadJson(jsonPath))
        {
            metadata = Launcher.Serializer.Deserialize<ClientMetadata>(reader);
        }

        Debug.Assert(metadata != null);

        await obj.PerformTaskAsync(new RestoreClientTask(metadata.Id, metadata.Downloads["client"], AmclApp.StorageConfig));

        obj.UpdateMessage("Saving installation");
        obj.UpdateProgress(-1);
        Installation.Save(new Installation(name, metadata.Id), AmclApp.StorageConfig.Repository);

        try
        {
            obj.UpdateMessage("Restoring assets");
            await obj.PerformTaskAsync(new RestoreAssetsTask(metadata.AssetIndex, AmclApp.StorageConfig));

            obj.UpdateMessage("Restoring libraries");
            await obj.PerformTaskAsync(new RestoreLibrariesTask(metadata.Libraries, AmclApp.StorageConfig.Repository.GetNativesPath(metadata.Id), AmclApp.StorageConfig));
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            MessageBox.Show($"Installation created with error: {ex.Message}.\r\nAssets and libraries will be restored during launch.",
                "Warning",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning);
        }
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
        cancelWork = true;
        Close();
    }
}
