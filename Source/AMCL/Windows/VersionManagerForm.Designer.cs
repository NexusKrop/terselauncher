﻿namespace AMCL.Windows;

partial class VersionManagerForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        listVersions = new ListView();
        headerName = new ColumnHeader();
        headerVersion = new ColumnHeader();
        buttonCreate = new Button();
        buttonDelete = new Button();
        SuspendLayout();
        // 
        // listVersions
        // 
        listVersions.BackColor = Color.FromArgb(45, 45, 45);
        listVersions.BorderStyle = BorderStyle.FixedSingle;
        listVersions.Columns.AddRange(new ColumnHeader[] { headerName, headerVersion });
        listVersions.ForeColor = Color.White;
        listVersions.Location = new Point(12, 43);
        listVersions.Name = "listVersions";
        listVersions.Size = new Size(776, 395);
        listVersions.TabIndex = 0;
        listVersions.UseCompatibleStateImageBehavior = false;
        listVersions.View = View.Details;
        // 
        // headerName
        // 
        headerName.Text = "Name";
        headerName.Width = 300;
        // 
        // headerVersion
        // 
        headerVersion.Text = "Version";
        headerVersion.Width = 150;
        // 
        // buttonCreate
        // 
        buttonCreate.BackColor = Color.ForestGreen;
        buttonCreate.FlatAppearance.BorderColor = Color.FromArgb(0, 64, 0);
        buttonCreate.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 64, 0);
        buttonCreate.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 192, 0);
        buttonCreate.FlatStyle = FlatStyle.Flat;
        buttonCreate.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
        buttonCreate.ForeColor = Color.White;
        buttonCreate.Location = new Point(713, 6);
        buttonCreate.Name = "buttonCreate";
        buttonCreate.Size = new Size(75, 31);
        buttonCreate.TabIndex = 1;
        buttonCreate.Text = "Create";
        buttonCreate.UseVisualStyleBackColor = false;
        buttonCreate.Click += buttonCreate_Click;
        // 
        // buttonDelete
        // 
        buttonDelete.BackColor = Color.FromArgb(64, 64, 64);
        buttonDelete.FlatAppearance.BorderColor = Color.Black;
        buttonDelete.FlatAppearance.MouseDownBackColor = Color.Black;
        buttonDelete.FlatAppearance.MouseOverBackColor = Color.Gray;
        buttonDelete.FlatStyle = FlatStyle.Flat;
        buttonDelete.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        buttonDelete.ForeColor = Color.White;
        buttonDelete.Location = new Point(632, 6);
        buttonDelete.Name = "buttonDelete";
        buttonDelete.Size = new Size(75, 31);
        buttonDelete.TabIndex = 4;
        buttonDelete.Text = "Delete";
        buttonDelete.UseVisualStyleBackColor = false;
        buttonDelete.Click += buttonDelete_Click;
        // 
        // VersionManagerForm
        // 
        AutoScaleDimensions = new SizeF(9F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(32, 32, 32);
        ClientSize = new Size(796, 450);
        Controls.Add(buttonDelete);
        Controls.Add(buttonCreate);
        Controls.Add(listVersions);
        FormBorderStyle = FormBorderStyle.FixedSingle;
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "VersionManagerForm";
        Text = "Installation Manager";
        Load += VersionManagerForm_Load;
        ResumeLayout(false);
    }

    #endregion

    private ListView listVersions;
    private ColumnHeader headerName;
    private ColumnHeader headerVersion;
    private Button buttonCreate;
    private Button buttonDelete;
}