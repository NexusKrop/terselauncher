﻿namespace AMCL.Windows;

partial class SettingsForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        listOptions = new ListBox();
        panelDock = new Panel();
        panelResult = new Panel();
        buttonOk = new Button();
        buttonCancel = new Button();
        panelResult.SuspendLayout();
        SuspendLayout();
        // 
        // listOptions
        // 
        listOptions.BackColor = Color.FromArgb(45, 45, 45);
        listOptions.BorderStyle = BorderStyle.FixedSingle;
        listOptions.ForeColor = Color.White;
        listOptions.FormattingEnabled = true;
        listOptions.ItemHeight = 20;
        listOptions.Items.AddRange(new object[] { "General", "Launcher", "Java", "Minecraft" });
        listOptions.Location = new Point(11, 12);
        listOptions.Name = "listOptions";
        listOptions.Size = new Size(176, 382);
        listOptions.TabIndex = 0;
        listOptions.SelectedIndexChanged += listOptions_SelectedIndexChanged;
        // 
        // panelDock
        // 
        panelDock.BackColor = Color.FromArgb(45, 45, 45);
        panelDock.Location = new Point(192, 12);
        panelDock.Name = "panelDock";
        panelDock.Size = new Size(508, 382);
        panelDock.TabIndex = 1;
        // 
        // panelResult
        // 
        panelResult.BackColor = Color.FromArgb(43, 43, 43);
        panelResult.Controls.Add(buttonOk);
        panelResult.Controls.Add(buttonCancel);
        panelResult.Location = new Point(0, 400);
        panelResult.Name = "panelResult";
        panelResult.Size = new Size(716, 55);
        panelResult.TabIndex = 5;
        // 
        // buttonOk
        // 
        buttonOk.BackColor = Color.ForestGreen;
        buttonOk.FlatAppearance.BorderColor = Color.FromArgb(0, 64, 0);
        buttonOk.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 64, 0);
        buttonOk.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 192, 0);
        buttonOk.FlatStyle = FlatStyle.Flat;
        buttonOk.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
        buttonOk.ForeColor = Color.White;
        buttonOk.Location = new Point(625, 7);
        buttonOk.Name = "buttonOk";
        buttonOk.Size = new Size(75, 31);
        buttonOk.TabIndex = 2;
        buttonOk.Text = "OK";
        buttonOk.UseVisualStyleBackColor = false;
        buttonOk.Click += buttonOk_Click;
        // 
        // buttonCancel
        // 
        buttonCancel.BackColor = Color.FromArgb(64, 64, 64);
        buttonCancel.FlatAppearance.BorderColor = Color.Black;
        buttonCancel.FlatAppearance.MouseDownBackColor = Color.Black;
        buttonCancel.FlatAppearance.MouseOverBackColor = Color.Gray;
        buttonCancel.FlatStyle = FlatStyle.Flat;
        buttonCancel.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        buttonCancel.ForeColor = Color.White;
        buttonCancel.Location = new Point(543, 7);
        buttonCancel.Name = "buttonCancel";
        buttonCancel.Size = new Size(75, 31);
        buttonCancel.TabIndex = 3;
        buttonCancel.Text = "Cancel";
        buttonCancel.UseVisualStyleBackColor = false;
        buttonCancel.Click += buttonCancel_Click;
        // 
        // SettingsForm
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(32, 32, 32);
        ClientSize = new Size(711, 450);
        Controls.Add(panelResult);
        Controls.Add(panelDock);
        Controls.Add(listOptions);
        Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        ForeColor = Color.White;
        FormBorderStyle = FormBorderStyle.FixedSingle;
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "SettingsForm";
        Text = "Settings";
        Load += SettingsForm_Load;
        panelResult.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private ListBox listOptions;
    private Panel panelDock;
    private Panel panelResult;
    private Button buttonOk;
    private Button buttonCancel;
}