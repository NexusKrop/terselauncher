﻿namespace AMCL.Windows.Controls;

partial class NoticeBar
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        panelLayout = new TableLayoutPanel();
        labelHeader = new Label();
        labelDescription = new Label();
        panelLayout.SuspendLayout();
        SuspendLayout();
        // 
        // panelLayout
        // 
        panelLayout.ColumnCount = 1;
        panelLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
        panelLayout.Controls.Add(labelHeader, 0, 0);
        panelLayout.Controls.Add(labelDescription, 0, 1);
        panelLayout.Dock = DockStyle.Fill;
        panelLayout.Location = new Point(0, 0);
        panelLayout.Name = "panelLayout";
        panelLayout.RowCount = 2;
        panelLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
        panelLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
        panelLayout.Size = new Size(358, 96);
        panelLayout.TabIndex = 0;
        // 
        // labelHeader
        // 
        labelHeader.AutoSize = true;
        labelHeader.BackColor = Color.FromArgb(224, 9, 9);
        labelHeader.Dock = DockStyle.Fill;
        labelHeader.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
        labelHeader.ForeColor = Color.White;
        labelHeader.Location = new Point(0, 0);
        labelHeader.Margin = new Padding(0);
        labelHeader.Name = "labelHeader";
        labelHeader.Padding = new Padding(3, 0, 3, 0);
        labelHeader.Size = new Size(358, 25);
        labelHeader.TabIndex = 0;
        labelHeader.Text = "Warning";
        labelHeader.TextAlign = ContentAlignment.MiddleLeft;
        // 
        // labelDescription
        // 
        labelDescription.BackColor = Color.FromArgb(178, 15, 15);
        labelDescription.Dock = DockStyle.Fill;
        labelDescription.ForeColor = Color.White;
        labelDescription.Location = new Point(0, 25);
        labelDescription.Margin = new Padding(0);
        labelDescription.Name = "labelDescription";
        labelDescription.Padding = new Padding(3, 0, 3, 0);
        labelDescription.Size = new Size(358, 71);
        labelDescription.TabIndex = 1;
        labelDescription.Text = "Description";
        // 
        // NoticeBar
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(255, 128, 0);
        Controls.Add(panelLayout);
        Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        MinimumSize = new Size(269, 96);
        Name = "NoticeBar";
        Size = new Size(358, 96);
        panelLayout.ResumeLayout(false);
        panelLayout.PerformLayout();
        ResumeLayout(false);
    }

    #endregion

    private TableLayoutPanel panelLayout;
    private Label labelHeader;
    private Label labelDescription;
}
