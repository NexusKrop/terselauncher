﻿namespace AMCL.Windows.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

[DesignTimeVisible(true)]
[DesignerCategory("Common Controls")]
public partial class NoticeBar : UserControl
{
    public NoticeBar()
    {
        InitializeComponent();
    }

    public string Title
    {
        get => labelHeader.Text;
        set => labelHeader.Text = value;
    }

    public string Description
    {
        get => labelDescription.Text;
        set => labelDescription.Text = value;
    }
}
