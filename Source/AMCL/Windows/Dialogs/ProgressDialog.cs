﻿namespace AMCL.Windows.Dialogs;
using System;
using System.Diagnostics;
using System.Windows.Forms;
using AMCL.Core.Tasks;

public partial class ProgressDialog : Form
{
    private readonly Thread _executionThread;
    private readonly Func<ProgressDialog, Task> _pTask;

    public ProgressDialog(Func<ProgressDialog, Task> task)
    {
        InitializeComponent();

        _pTask = task;
        _executionThread = new Thread(Perform);

        TaskPerformer = new(Log, UpdateProgress, UpdateMessage);
    }

    public bool Success { get; private set; }
    public bool Running { get; private set; }

    public TaskPerformer TaskPerformer { get; }

    public async Task<T> PerformTaskAsync<T>(IResultingTask<T> task)
    {
        return await task.Execute(TaskPerformer);
    }

    public async Task PerformTaskAsync(ITask task)
    {
        await task.Execute(TaskPerformer);
    }

    private void Complete()
    {
        Success = true;

        Invoke(Close);
    }

    public void UpdateMessage(string message)
    {
        Invoke(() => labelUpdate.Text = message);
    }

    public void Log(string message)
    {
        Invoke(() =>
        {
            textLog.AppendText(message);
            textLog.AppendText(Environment.NewLine);
        });
    }

    public void UpdateProgress(int progress)
    {
        if (progress < 0)
        {
            Invoke(() => progressBar1.Style = ProgressBarStyle.Marquee);
        }
        else
        {
            Invoke(() =>
            {
                progressBar1.Style = ProgressBarStyle.Continuous;
                progressBar1.Value = progress;
            });
        }
    }

    private void Perform()
    {
        Running = true;

        try
        {
            _pTask.Invoke(this).GetAwaiter().GetResult();
        }
        catch (UnauthorizedAccessException ex)
        {
            Debug.WriteLine(ex);
            MessageBox.Show($"{ex.Message}\r\nPlease check if you have the read and write access to the directory or file above, and the directory or file above is not read-only.",
                "Failed to perform task",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }
        catch (Exception ex)
        {
            Invoke(() =>
            {
                MessageBox.Show(ex.ToString(), "Failed to perform task");
                Close();
            });

            Success = false;
            return;
        }

        Complete();
    }

    private void ProgressDialog_Load(object sender, EventArgs e)
    {
        _executionThread.Start();
    }
}
