﻿namespace AMCL.Windows.Dialogs;

partial class ProgressDialog
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgressDialog));
        labelUpdate = new Label();
        progressBar1 = new ProgressBar();
        textLog = new TextBox();
        SuspendLayout();
        // 
        // labelUpdate
        // 
        labelUpdate.Location = new Point(9, 9);
        labelUpdate.Margin = new Padding(0, 0, 3, 0);
        labelUpdate.Name = "labelUpdate";
        labelUpdate.Size = new Size(779, 25);
        labelUpdate.TabIndex = 0;
        labelUpdate.Text = "Operation in progress..";
        // 
        // progressBar1
        // 
        progressBar1.Location = new Point(12, 42);
        progressBar1.Name = "progressBar1";
        progressBar1.Size = new Size(776, 19);
        progressBar1.TabIndex = 1;
        // 
        // textLog
        // 
        textLog.BackColor = Color.FromArgb(43, 43, 43);
        textLog.BorderStyle = BorderStyle.FixedSingle;
        textLog.ForeColor = Color.White;
        textLog.Location = new Point(12, 67);
        textLog.Multiline = true;
        textLog.Name = "textLog";
        textLog.ReadOnly = true;
        textLog.Size = new Size(776, 313);
        textLog.TabIndex = 2;
        // 
        // ProgressDialog
        // 
        AutoScaleDimensions = new SizeF(9F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(32, 32, 32);
        ClientSize = new Size(800, 392);
        ControlBox = false;
        Controls.Add(textLog);
        Controls.Add(progressBar1);
        Controls.Add(labelUpdate);
        ForeColor = Color.White;
        FormBorderStyle = FormBorderStyle.FixedDialog;
        Icon = (Icon)resources.GetObject("$this.Icon");
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "ProgressDialog";
        Text = "Progress";
        Load += ProgressDialog_Load;
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Label labelUpdate;
    private ProgressBar progressBar1;
    private TextBox textLog;
}