﻿namespace AMCL.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AMCL.Windows.Settings;

public partial class SettingsForm : Form
{
    private UserControl? _current;

    private readonly Dictionary<int, UserControl> _pages = new()
    {
        { 0, new GeneralPage() },
        { 1, new UserControl() },
        { 2, new UserControl() },
        { 3, new MinecraftPage() },
    };

    public SettingsForm()
    {
        InitializeComponent();
    }

    private void SettingsForm_Load(object sender, EventArgs e)
    {
        listOptions.SelectedIndex = 0;
    }

    private void listOptions_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (_current != null)
        {
            _current.Visible = false;
            _current = null;
        }

        _current = _pages[listOptions.SelectedIndex];

        if (!panelDock.Controls.Contains(_current))
        {
            panelDock.Controls.Add(_current);
        }

        _current.Visible = true;
        _current.Dock = DockStyle.Fill;
    }

    private void buttonOk_Click(object sender, EventArgs e)
    {
        foreach (var page in _pages.Values)
        {
            if (page is ISettingsPage x)
            {
                x.Save();
            }
        }

        Properties.Settings.Default.Save();
        AmclApp.Update();
        Close();
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
        Close();
    }
}
