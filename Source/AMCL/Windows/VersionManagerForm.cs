﻿namespace AMCL.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AMCL.Core.Launch;
using AMCL.Repository;

public partial class VersionManagerForm : Form
{
    private IList<Installation>? _installations;

    public VersionManagerForm()
    {
        InitializeComponent();
    }

    public void ReloadVersions()
    {
        listVersions.Items.Clear();

        _installations = Installation.ScanForInstallations(AmclApp.StorageConfig.Repository);

        foreach (var installation in _installations)
        {
            var item = new ListViewItem(installation.Name);
            item.SubItems.Add(new ListViewItem.ListViewSubItem()
            {
                Text = installation.Version
            });
            item.Tag = installation;
            listVersions.Items.Add(item);
        }
    }

    private void VersionManagerForm_Load(object sender, EventArgs e)
    {
        ReloadVersions();
    }

    private void buttonCreate_Click(object sender, EventArgs e)
    {
        this.Visible = false;
        new CreateInstallation().ShowDialog();
        this.Visible = true;

        ReloadVersions();
    }

    private void buttonDelete_Click(object sender, EventArgs e)
    {
        if (listVersions.SelectedItems.Count <= 0)
        {
            return;
        }

        if (MessageBox.Show("This will delete everything (saves, mods, configs, screenshots, etc.) stored in the selected installations.\r\nAre you sure you want to delete the selected installations?",
            "Warning",
            MessageBoxButtons.YesNo,
            MessageBoxIcon.Warning) != DialogResult.Yes)
        {
            return;
        }

        var list = listVersions.SelectedItems;
        for (int i = 0; i < list.Count; i++)
        {
            object? item = list[i];
            var x = (ListViewItem)item;

            if (x.Tag is not Installation install)
            {
                listVersions.Items.RemoveAt(i);
                continue;
            }

            var target = Path.Combine(AmclApp.StorageConfig.Repository.UserPath, install.Name);

            if (Directory.Exists(target))
            {
                Directory.Delete(target, true);
            }
        }

        ReloadVersions();
    }
}
