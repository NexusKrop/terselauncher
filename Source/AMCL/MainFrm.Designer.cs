﻿namespace AMCL;

partial class MainFrm
{
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        panelControls = new Panel();
        darkVersions = new DarkUI.Controls.DarkButton();
        listVersion = new DarkUI.Controls.DarkDropdownList();
        buttonPlay = new Button();
        panelTop = new Panel();
        buttonSettings = new DarkUI.Controls.DarkButton();
        panelControls.SuspendLayout();
        panelTop.SuspendLayout();
        SuspendLayout();
        // 
        // panelControls
        // 
        panelControls.BackColor = Color.FromArgb(32, 32, 32);
        panelControls.Controls.Add(darkVersions);
        panelControls.Controls.Add(listVersion);
        panelControls.Controls.Add(buttonPlay);
        panelControls.Location = new Point(0, 211);
        panelControls.Name = "panelControls";
        panelControls.Size = new Size(729, 116);
        panelControls.TabIndex = 0;
        // 
        // darkVersions
        // 
        darkVersions.Location = new Point(165, 71);
        darkVersions.Name = "darkVersions";
        darkVersions.Padding = new Padding(5);
        darkVersions.Size = new Size(94, 32);
        darkVersions.TabIndex = 2;
        darkVersions.Text = "Manage";
        darkVersions.Click += darkVersions_Click;
        // 
        // listVersion
        // 
        listVersion.Location = new Point(12, 71);
        listVersion.Name = "listVersion";
        listVersion.Size = new Size(147, 32);
        listVersion.TabIndex = 1;
        listVersion.Text = "darkDropdownList1";
        // 
        // buttonPlay
        // 
        buttonPlay.BackColor = Color.ForestGreen;
        buttonPlay.FlatAppearance.BorderColor = Color.FromArgb(0, 64, 0);
        buttonPlay.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 64, 0);
        buttonPlay.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 192, 0);
        buttonPlay.FlatStyle = FlatStyle.Flat;
        buttonPlay.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
        buttonPlay.ForeColor = Color.White;
        buttonPlay.Location = new Point(564, 58);
        buttonPlay.Name = "buttonPlay";
        buttonPlay.Size = new Size(153, 45);
        buttonPlay.TabIndex = 0;
        buttonPlay.Text = "Play";
        buttonPlay.UseVisualStyleBackColor = false;
        // 
        // panelTop
        // 
        panelTop.BackColor = Color.FromArgb(32, 32, 32);
        panelTop.Controls.Add(buttonSettings);
        panelTop.Location = new Point(0, 0);
        panelTop.Name = "panelTop";
        panelTop.Size = new Size(729, 34);
        panelTop.TabIndex = 1;
        // 
        // buttonSettings
        // 
        buttonSettings.Location = new Point(0, 0);
        buttonSettings.Name = "buttonSettings";
        buttonSettings.Padding = new Padding(5);
        buttonSettings.Size = new Size(94, 34);
        buttonSettings.TabIndex = 0;
        buttonSettings.Text = "Settings";
        buttonSettings.Click += buttonSettings_Click;
        // 
        // MainFrm
        // 
        AutoScaleDimensions = new SizeF(8F, 20F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(45, 45, 45);
        ClientSize = new Size(729, 326);
        Controls.Add(panelTop);
        Controls.Add(panelControls);
        Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
        MaximizeBox = false;
        Name = "MainFrm";
        Text = "AMCL";
        Load += MainFrm_Load;
        panelControls.ResumeLayout(false);
        panelTop.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private Panel panelControls;
    private Button buttonPlay;
    private DarkUI.Controls.DarkDropdownList listVersion;
    private Panel panelTop;
    private DarkUI.Controls.DarkButton darkVersions;
    private DarkUI.Controls.DarkButton buttonSettings;
}
