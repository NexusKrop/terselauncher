﻿namespace AMCL.Repository;
using System.Collections.Generic;
using AMCL.Core.Launch;

public class Installation
{
    public Installation(string name, string version)
    {
        Name = name;
        Version = version;
    }

    public string Version { get; set; }
    public string Name { get; set; }

    public static void Save(Installation installation, GameRepository repository)
    {
        var targetDir = Path.Combine(repository.UserPath, installation.Name);
        Directory.CreateDirectory(targetDir);

        using var writer = Launcher.CreateJson(Path.Combine(targetDir, "install.json"));
        Launcher.Serializer.Serialize(writer, installation);
    }

    public static IList<Installation> ScanForInstallations(GameRepository repository)
    {
        var result = new List<Installation>();

        if (!Directory.Exists(repository.UserPath))
        {
            // Return an empty list
            return result;
        }

        foreach (var subdir in Directory.GetDirectories(repository.UserPath))
        {
            var installJson = Path.Combine(subdir, "install.json");

            if (!File.Exists(installJson))
            {
                continue;
            }

            Installation? x;

            using (var reader = Launcher.OpenReadJson(installJson))
            {
                x = Launcher.Serializer.Deserialize<Installation>(reader);
            }

            if (x == null)
            {
                continue;
            }

            result.Add(x);
        }

        return result;
    }
}
