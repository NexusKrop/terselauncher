namespace AMCL;

using AMCL.Properties;
using AMCL.Windows;

public partial class MainFrm : Form
{
    public MainFrm()
    {
        InitializeComponent();
    }

    private void MainFrm_Load(object sender, EventArgs e)
    {
        AmclApp.StorageConfig.Repository.CreateAll();
    }

    private void darkVersions_Click(object sender, EventArgs e)
    {
        Visible = false;
        new VersionManagerForm().ShowDialog();
        Visible = true;
    }

    private void buttonSettings_Click(object sender, EventArgs e)
    {
        this.Visible = false;
        new SettingsForm().ShowDialog();
        this.Visible = true;
    }
}
