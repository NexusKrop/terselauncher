﻿namespace AMCL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMCL.Core.Launch;
using AMCL.Core.Network;
using AMCL.Properties;

public static class AmclApp
{
    static AmclApp()
    {
        Mirror = GetCurrentMirror();
        StorageConfig = GetStorageConfig();
    }

    public static bool LicencesOk { get; private set; }
    public static readonly string LicencesPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath)!, "LICENCES.txt");

    public static GameStorageConfig StorageConfig { get; private set; }
    public static IMirrorProvider Mirror { get; private set; }

    public static void SetLicences()
    {
        if (!File.Exists(LicencesPath))
        {
            try
            {
                File.WriteAllBytes(LicencesPath, Resources.CREDITS);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return;
            }
        }

        LicencesOk = true;
    }

    public static void CreateDirectorySafe(string path)
    {
        if (Directory.Exists(path))
        {
            return;
        }

        var result = Directory.CreateDirectory(path);
    }

    public static GameStorageConfig GetStorageConfig()
    {
        var repo = GameRepository.Default;

        if (!string.IsNullOrWhiteSpace(Settings.Default.RepositoryLocation))
        {
            repo = new GameRepository(Settings.Default.RepositoryLocation);
        }

        return new(repo, Mirror);
    }

    private static IMirrorProvider GetCurrentMirror()
    {
        return Settings.Default.Mirror switch
        {
            1 => new BmclApiMirrorProvider(),
            _ => new MojangMirrorProvider(),
        };
    }

    public static void Update()
    {
        Mirror = GetCurrentMirror();
        StorageConfig = GetStorageConfig();
    }
}
