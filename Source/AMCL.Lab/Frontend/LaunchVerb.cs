﻿namespace AMCL.Lab.Frontend;

using CommandLine;
using DaanV2.UUID;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using AMCL.Core.Launch;
using AMCL.Core.Network;
using AMCL.Core.Resources.Game;
using AMCL.Core.Resources.Game.Client;
using AMCL.Core.Tasks;
using AMCL.Lab.Tasks;

[Verb("launch", isDefault: true)]
public class LaunchVerb
{
    private static readonly GameStorageConfig StorageConfig = new(GameRepository.Default,
        new BmclApiMirrorProvider());

    [Option('d', "demo")]
    public bool DemoMode { get; set; }

    [Option('n', "player-name", Default = "Player")]
    public string PlayerName { get; set; } = "Player";

    [Option("max-memory", Default = 2048, HelpText = "Maximum memory amount in MB")]
    public int MaxMemory { get; set; } = 2048;

    public static async Task Execute(LaunchVerb arg)
    {
        Debug.Assert(StorageConfig.Repository.BasePath != null);

        StorageConfig.Repository.CreateAll();
        StorageConfig.Repository.PrintAll();

        Console.WriteLine("Welcome to TerseLauncher!");
        Console.WriteLine();

        ParseClientTask parse;
        ClientMetadata metadata;

        using (var reader = new JsonTextReader(new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(DebugResources._1_16_5)))))
        {
            parse = new ParseClientTask(arg.PlayerName,
            arg.DemoMode,
            reader,
            StorageConfig);

            metadata = await Launcher.ExecuteTask(parse);
        }

        var assets = metadata.AssetIndex;

        var restoreAssets = new RestoreAssetsTask(assets, StorageConfig);

        var natives = StorageConfig.Repository.GetNativesPath(metadata.Id);

        var restoreClient = new RestoreClientTask(metadata.Id, metadata.Downloads["client"], StorageConfig);
        var restoreLog = new RestoreLogTask(metadata.Id, metadata.Logging["client"], StorageConfig);

        var restoreLibraries = new RestoreLibrariesTask(metadata.Libraries, natives, StorageConfig);


        await Launcher.ExecuteTask(restoreAssets);
        await Launcher.ExecuteTask(restoreLog);
        await Launcher.ExecuteTask(restoreClient);
        await Launcher.ExecuteTask(restoreLibraries);

        var info = new LaunchInfo(metadata: metadata,
            userName: arg.PlayerName,
            uuid: UUIDFactory.CreateUUID(3, 1, $"OfflinePlayer:{arg.PlayerName}"),
            logArgument: restoreLog.Argument!,
            classpath: restoreLibraries.Libraries,
            client: restoreClient.JarFile!,
            demo: arg.DemoMode,
            hasCustomResolution: false);

        var launch = new LaunchTask(repository: StorageConfig.Repository,
            launchInfo: info,
            jvm: @"D:\程序类\Java\jdk8u345-b01-jre\bin\java.exe")
        {
            MaxMemoryMb = arg.MaxMemory
        };

        await Launcher.ExecuteTask(launch);
    }
}
