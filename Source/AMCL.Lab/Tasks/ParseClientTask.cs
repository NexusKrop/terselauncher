﻿namespace AMCL.Lab.Tasks;

using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading.Tasks;
using AMCL.Core.Launch;
using AMCL.Core.Resources.Game.Client;
using AMCL.Core.Tasks;

internal class ParseClientTask : IResultingTask<ClientMetadata>
{
    private readonly JsonReader _reader;

    public ParseClientTask(string playerName, bool demo, JsonReader reader, GameStorageConfig storageConfig)
    {
        PlayerName = playerName;
        Demo = demo;

        _reader = reader;
        GameRepository = storageConfig.Repository;
    }

    public GameRepository GameRepository { get; }

    public string Id => "parseClient";

    public string PlayerName { get; set; }
    public bool Demo { get; set; }

    public async Task<ClientMetadata> Execute(TaskPerformer performer)
    {
        var result = await Task.Run(() => Launcher.Serializer.Deserialize<ClientMetadata>(_reader));
        Debug.Assert(result != null);
        return result;
    }
}
