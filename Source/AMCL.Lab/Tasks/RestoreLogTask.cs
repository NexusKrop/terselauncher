﻿namespace AMCL.Lab.Tasks;
using System;
using System.Threading.Tasks;
using AMCL.Core.Launch;
using AMCL.Core.Resources.LogJava;
using AMCL.Core.Tasks;

internal class RestoreLogTask : ITask
{
    public RestoreLogTask(string version, LogConfigPointer config, GameStorageConfig storageConfig)
    {
        _version = version;
        _pointer = config;

        GameRepository = storageConfig.Repository;
    }

    private readonly string _version;
    private readonly LogConfigPointer _pointer;

    public GameRepository GameRepository { get; }

    public string Id => "restoreLog";

    public string? Argument { get; set; }

    public async Task Execute(TaskPerformer performer)
    {
        var targetDir = Path.Combine(GameRepository.VersionPath, _version);
        var targetFile = Path.Combine(targetDir, _pointer.File.Id);

        Directory.CreateDirectory(targetDir);

        if (_pointer.Type == "log4j2-xml")
        {
            performer.Output("Using intergated log configuration file");
            if (!await Launcher.IsFileIntact(targetFile, DebugResources.CommandLineHash))
            {
                await File.WriteAllTextAsync(targetFile, DebugResources.CommandLineLog);
            }
        }
        else if (!await Launcher.IsFileIntact(targetFile, _pointer.File.Hash))
        {
            await Download(targetFile, performer);
        }

        Argument = _pointer.Argument.Replace("${path}", targetFile);
    }

    private async Task Download(string destination, TaskPerformer performer)
    {
        await Launcher.DownloadFileAsync(_pointer.File.Url, destination, _pointer.File.Size);
        performer.Output("Restored log configuration file via Internet");
    }
}
