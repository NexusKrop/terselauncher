﻿namespace AMCL.Lab.Tasks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using AMCL.Core.Launch;
using AMCL.Core.Resources.Game;
using AMCL.Core.Resources.Game.Client;
using AMCL.Core.Tasks;

internal class LaunchTask : ITask
{
    public string Id => "launch";

    public LaunchTask(GameRepository repository, LaunchInfo launchInfo, string jvm)
    {
        Repository = repository;
        LaunchInfo = launchInfo;
        Jvm = jvm;
    }

    public GameRepository Repository { get; set; }
    public LaunchInfo LaunchInfo { get; set; }
    public int MaxMemoryMb { get; set; } = 2048;
    public string Jvm { get; set; }

    public async Task Execute(TaskPerformer performer)
    {
        var processInfo = new ProcessStartInfo()
        {
            UseShellExecute = false,
            FileName = Jvm,
            RedirectStandardOutput = true
        };

        performer.Output("Assembling classpath");

        var classpath = AssmbleClasspath();

        performer.Output("Generating arguments");

        // Generate JVM arguments
        var jvmArgs = ArgumentGeneration.GenerateJvm(LaunchInfo.ClientMetadata.Arguments.Jvm,
            ArgumentGeneration.GenerateJvmAnswers(LaunchInfo, Repository));

        // Generate game arguments
        var gameArgs = ArgumentGeneration.GenerateGame(LaunchInfo.ClientMetadata.Arguments.Game,
            LaunchInfo,
            ArgumentGeneration.GenerateGameAnswers(LaunchInfo, Repository));

        // Command line arguments come in order of:
        //  1. Perferences (Xmx, etc.)
        //  2. JSON JVM Arguments
        //  3. JSON Log Arguments
        //  4. Main Class Name
        //  5. JSON Game Arguments

        // Perferences
        processInfo.ArgumentList.Add(string.Format("-Xmx{0}M", MaxMemoryMb));

        // JVM Arguments
        foreach (var jvmArg in jvmArgs)
        {
            if (jvmArg == Launcher.ClasspathPlaceholder)
            {
                processInfo.ArgumentList.Add(classpath);
            }
            else
            {
                processInfo.ArgumentList.Add(jvmArg);
            }
        }

        // Log arguments
        processInfo.ArgumentList.Add(LaunchInfo.LogArgument);

        // Main class name
        processInfo.ArgumentList.Add(LaunchInfo.ClientMetadata.MainClass);

        // Game arguments
        foreach (var gameArg in gameArgs)
        {
            processInfo.ArgumentList.Add(gameArg);
        }

        var process = Process.Start(processInfo);

        if (process == null)
        {
            performer.Output("Start failed");
            return;
        }

        process.OutputDataReceived += (sender, args) =>
        {
            performer.Output(args.Data);
        };

        await process.WaitForExitAsync();
    }

    private string AssmbleClasspath()
    {
        var builder = new StringBuilder();

        foreach (var cp in LaunchInfo.Classpath)
        {
            builder.Append(cp).Append(';');
        }

        builder.Append(LaunchInfo.ClientExecutable);

        return builder.ToString();
    }
}
