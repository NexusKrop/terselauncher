﻿using CommandLine;
using Crayon;
using System.Diagnostics;
using AMCL.Lab.Frontend;

Crayon.Output.Enable();

try
{
    await Parser.Default.ParseArguments<LaunchVerb>(args)
        .WithParsedAsync(LaunchVerb.Execute);
}
catch (Exception ex)
{
    Console.WriteLine(Output.Red(ex.ToString()));

    if (Debugger.IsAttached)
    {
        throw;
    }

    return -2;
}

return 0;
