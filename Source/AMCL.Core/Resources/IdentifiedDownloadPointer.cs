﻿namespace AMCL.Core.Resources;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public record struct IdentifiedDownloadPointer
{
    [JsonProperty("sha1")]
    public string Hash { get; set; }
    public int Size { get; set; }
    public string Url { get; set; }
    public string Id { get; set; }
}
