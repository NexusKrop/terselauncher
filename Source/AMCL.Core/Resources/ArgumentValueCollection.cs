﻿namespace AMCL.Core.Resources;
using System.Collections;
using System.Collections.Generic;

public class ArgumentValueCollection : IList<string>
{
    public ArgumentValueCollection(string value)
    {
        _strings = new()
        {
            value
        };
    }

    public ArgumentValueCollection()
    {
        _strings = new();
    }

    private readonly List<string> _strings;

    public int Count => _strings.Count;

    public bool IsReadOnly => false;

    public string this[int index] { get => _strings[index]; set => _strings[index] = value; }

    public void Add(string item)
    {
        _strings.Add(item);
    }

    public void Clear()
    {
        _strings.Clear();
    }

    public bool Contains(string item)
    {
        return _strings.Contains(item);
    }

    public void CopyTo(string[] array, int arrayIndex)
    {
        _strings.CopyTo(array, arrayIndex);
    }

    public bool Remove(string item)
    {
        return _strings.Remove(item);
    }

    public IEnumerator<string> GetEnumerator()
    {
        return _strings.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _strings.GetEnumerator();
    }

    public int IndexOf(string item)
    {
        return _strings.IndexOf(item);
    }

    public void Insert(int index, string item)
    {
        _strings.Insert(index, item);
    }

    public void RemoveAt(int index)
    {
        _strings.RemoveAt(index);
    }

    public static implicit operator ArgumentValueCollection(string value)
    {
        return new ArgumentValueCollection(value);
    }
}
