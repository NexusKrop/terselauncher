﻿namespace AMCL.Core.Resources.LogJava;

using Newtonsoft.Json;

public record struct LogConfigPointer
{
    public IdentifiedDownloadPointer File { get; set; }

    public string Argument { get; set; }
    public string Type { get; set; }
}
