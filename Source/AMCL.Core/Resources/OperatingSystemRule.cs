﻿namespace AMCL.Core.Resources;
using System;
using System.Text.RegularExpressions;

public class OperatingSystemRule
{
    public string? Name { get; set; }
    public string? Version { get; set; }
    public string? Arch { get; set; }

    public bool Verify()
    {
        var result = true;

        if (Name != null)
        {
            result = OperatingSystem.IsOSPlatform(Name);
        }

        if (Version != null)
        {
            var actualVersion = Environment.OSVersion.Version.ToString();

            result = Regex.Match(actualVersion, Version).Success;
        }

        if (Arch != null)
        {
            var alt = Environment.Is64BitOperatingSystem ? "x64" : "x86";

            result = Arch.Equals(alt, StringComparison.OrdinalIgnoreCase); // FIXME this could not work on ARMs
        }

        return result;
    }
}
