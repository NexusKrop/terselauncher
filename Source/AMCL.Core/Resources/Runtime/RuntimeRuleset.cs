﻿#nullable disable
namespace AMCL.Core.Resources.Runtime;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class RuntimeRuleset
{
    public OperatingSystemRule OS { get; set; }

    [JsonConverter(typeof(StringEnumConverter))]
    public RuleAction Action { get; set; }
}
