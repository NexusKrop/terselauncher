﻿namespace AMCL.Core.Resources.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMCL.Core.Resources.Game.Client;

public class RuntimeArgument
{
    public RuntimeArgument()
    {
        Value = new();
    }
    public RuntimeArgument(string value)
    {
        Value = new ArgumentValueCollection(value);
    }

    public IList<RuntimeRuleset>? Rules { get; set; }
    public ArgumentValueCollection Value { get; set; }

    public static implicit operator RuntimeArgument(string value)
    {
        return new RuntimeArgument(value);
    }
}
