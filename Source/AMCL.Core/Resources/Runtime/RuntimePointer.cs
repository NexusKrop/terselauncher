﻿namespace AMCL.Core.Resources.Runtime;

public record struct RuntimePointer
{
    public RuntimePointer(string component, int javaVersion)
    {
        Component = component;
        JavaVersion = javaVersion;
    }

    public string Component { get; set; }
    public int JavaVersion { get; set; }
}
