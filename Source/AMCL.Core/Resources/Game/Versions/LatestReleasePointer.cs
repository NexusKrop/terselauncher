﻿namespace AMCL.Core.Resources.Game.Versions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public record struct LatestReleasePointer
{
    public string Release { get; set; }
    public string Snapshot { get; set; }
}
