﻿namespace AMCL.Core.Resources.Game.Versions;
using System.Collections.Generic;

public class ClientVersionManifest
{
    public ClientVersionManifest(LatestReleasePointer latest, IList<ClientMetadataPointer> versions)
    {
        Latest = latest;
        Versions = versions;
    }

    public LatestReleasePointer Latest { get; set; }
    public IList<ClientMetadataPointer> Versions { get; set; }
}
