﻿namespace AMCL.Core.Resources.Game.Versions;
using System;

public record ClientMetadataPointer
{
    public string Id { get; set; }
    public string Type { get; set; }
    public string Url { get; set; }
    public int ComplianceLevel { get; set; }
    public string Sha1 { get; set; }
    public DateTime ReleaseTime { get; set; }
    public DateTime Time { get; set; }

    public override string ToString()
    {
        return Id;
    }
}
