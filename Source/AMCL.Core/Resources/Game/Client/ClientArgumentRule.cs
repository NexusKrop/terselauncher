﻿namespace AMCL.Core.Resources.Game.Client;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AMCL.Core.Launch;

public class ClientArgumentRule
{
    public IDictionary<string, bool>? Features { get; set; }

    [JsonConverter(typeof(StringEnumConverter))]
    public RuleAction Action { get; set; }
}
