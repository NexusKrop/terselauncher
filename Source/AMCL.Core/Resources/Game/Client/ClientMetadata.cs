﻿#nullable disable

namespace AMCL.Core.Resources.Game.Client;

using Newtonsoft.Json;
using AMCL.Core.Resources.Assets;
using AMCL.Core.Resources.Libraries;
using AMCL.Core.Resources.LogJava;
using AMCL.Core.Resources.Runtime;

public class ClientMetadata
{
    public ClientArgumentSet Arguments { get; set; }
    public AssetsIndexPointer AssetIndex { get; set; }

    [JsonProperty("assets")]
    public string AssetsId { get; set; }

    public IDictionary<string, DownloadPointer> Downloads { get; set; }

    public int ComplianceLevel { get; set; }

    public string Id { get; set; }

    public RuntimePointer JavaVersion { get; set; }
    public IList<Library> Libraries { get; set; }

    public IDictionary<string, LogConfigPointer> Logging { get; set; }
    public string MainClass { get; set; }
    public int MinimumLauncherVersion { get; set; }
    public DateTime ReleaseTime { get; set; }
    public DateTime Time { get; set; }
    public string Type { get; set; }
}
