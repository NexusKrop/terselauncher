﻿namespace AMCL.Core.Resources.Game.Client;
using System.Collections.Generic;
using AMCL.Core.Resources.Runtime;

public class ClientArgumentSet
{
    public ClientArgumentSet(IList<ClientArgument> game, IList<RuntimeArgument> jvm)
    {
        Game = game;
        Jvm = jvm;
    }

    public IList<ClientArgument> Game { get; set; }
    public IList<RuntimeArgument> Jvm { get; set; }
}
