﻿namespace AMCL.Core.Resources.Game.Client;

using Newtonsoft.Json;
using System.Text.Json;

public class ClientRuleset
{
    [JsonProperty(Required = Required.DisallowNull)]
    public IDictionary<string, bool>? Features { get; set; }

    public string Action { get; set; }

    public static bool CheckGame(JsonElement argument, bool isDemo, bool customResolution)
    {
        var rules = argument.GetProperty("rules");

        var allow = true;

        foreach (var rule in rules.EnumerateArray())
        {
            if (!allow)
            {
                break;
            }

            var success = false;
            var features = rule.GetProperty("features");

            success = customResolution && features.TryGetProperty("has_custom_resolution", out var hasCustomReso) && hasCustomReso.GetBoolean();
            success = isDemo && features.TryGetProperty("is_demo_user", out var demoUser) && demoUser.GetBoolean();

            allow = success;
        }

        return allow;
    }

    public static bool CheckJvm(JsonElement argument)
    {
        var rules = argument.GetProperty("rules");

        var allow = true;

        foreach (var rule in rules.EnumerateArray())
        {
            if (!allow)
            {
                break;
            }

            var features = rule.GetProperty("os");

            // Construct a rule and verify it.
            var xRule = new OperatingSystemRule();

            if (features.TryGetProperty("name", out var os))
            {
                xRule.Name = os.GetString();
            }

            if (features.TryGetProperty("version", out var version))
            {
                xRule.Version = version.GetString();
            }

            if (features.TryGetProperty("arch", out var arch))
            {
                xRule.Arch = arch.GetString();
            }

            // We will verify it here.
            allow = xRule.Verify();
        }

        return allow;
    }
}
