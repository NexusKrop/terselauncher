﻿namespace AMCL.Core.Resources.Game.Client;

using Newtonsoft.Json;
using System;
using System.Collections.Immutable;
using AMCL.Core.Resources;

public class ClientArgument
{
    public ClientArgument() { }
    public ClientArgument(string value)
    {
        Value = new(value);
    }

    [JsonProperty(Required = Required.DisallowNull)]
    public IList<ClientRuleset>? Rules { get; set; }

    [JsonProperty(Required = Required.Always)]
    public ArgumentValueCollection Value { get; set; } = new();

    public static implicit operator ClientArgument(string value)
    {
        return new ClientArgument(value);
    }

    /// <summary>
    /// Searches the input <paramref name="argument"/> for all entries in <paramref name="answers"/> and apply the values if found.
    /// </summary>
    /// <param name="argument">The arguments to parse.</param>
    /// <param name="answers">The answers to fill.</param>
    /// <returns>The parsed argument.</returns>
    public static string Interpolate(string argument, IDictionary<string, string> answers)
    {
        if (!argument.Contains('$'))
        {
            // Nothing to search, no need for interpolation
            return argument;
        }

        if (argument.StartsWith("${") && argument.EndsWith('}'))
        {
            // This is a simple value argument, no need for strict interpolation
            return Parse(argument, answers);
        }

        var result = argument;

        foreach (var answer in answers)
        {
            var searchStr = string.Format("${{{0}}}", answer.Key);

            if (!result.Contains(searchStr))
            {
                continue;
            }

            result = result.Replace(searchStr, answer.Value);
        }

        return result;
    }

    public static string Parse(string argument, IDictionary<string, string> answers)
    {
        if (!argument.StartsWith("${") || !argument.EndsWith('}'))
        {
            return argument;
        }

        var key = argument.Remove(0, 2).Remove(argument.Length - 3, 1);

        if (answers.TryGetValue(key, out var value))
        {
            return value;
        }
        else
        {
            return "\"\"";
        }
    }
}
