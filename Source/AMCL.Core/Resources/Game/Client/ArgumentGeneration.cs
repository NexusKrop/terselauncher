﻿namespace AMCL.Core.Resources.Game.Client;

using DaanV2.UUID;
using System.Collections.Generic;
using System.Text;
using AMCL.Core.Launch;
using AMCL.Core.Resources.Assets;
using AMCL.Core.Resources.Runtime;

public static class ArgumentGeneration
{
    private static string AssembleClasspath(LaunchInfo info)
    {
        var builder = new StringBuilder();

        foreach (var cp in info.Classpath)
        {
            builder.Append(cp).Append(';');
        }

        builder.Append(info.ClientExecutable);

        return builder.ToString();
    }

    public static IDictionary<string, string> GenerateJvmAnswers(LaunchInfo info, GameRepository repository)
    {
        return new Dictionary<string, string>
        {
            { "launcher_name", Launcher.Name },
            { "launcher_version", Launcher.Version },
            { "classpath", AssembleClasspath(info) },
            { "natives_directory", repository.GetNativesPath(info.ClientMetadata.Id) }
        };
    }

    public static IDictionary<string, string> GenerateGameAnswers(LaunchInfo info, GameRepository repository)
    {
        return new Dictionary<string, string>
        {
            { "auth_player_name", info.UserName },
            { "auth_uuid", UUIDFactory.CreateUUID(3, 1, $"OfflinePlayer:{info.UserName}") },
            { "version_name", info.ClientMetadata.Id },
            { "version_type", info.ClientMetadata.Type},
            { "game_directory", repository.UserPath },
            { "assets_root", repository.AssetsPath },
            { "assets_index_name", info.ClientMetadata.AssetsId },
            { "auth_access_token", info.AccessToken },
            { "user_type", info.UserType },
        };
    }

    public static IList<string> GenerateJvm(IList<RuntimeArgument> arguments, IDictionary<string, string> answers)
    {
        var result = new List<string>();

        Console.WriteLine("Runtime arguments: ");
        foreach (var argument in arguments)
        {
            if (argument.Rules != null && !CheckRuntimeRules(argument.Rules))
            {
                continue;
            }

            foreach (var value in argument.Value)
            {
                var x = ClientArgument.Interpolate(value, answers);
                Console.Write("{0} ", x);
                result.Add(x);
            }
        }

        Console.WriteLine();

        return result;
    }

    public static IList<string> GenerateGame(IList<ClientArgument> arguments, LaunchInfo info, IDictionary<string, string> answers)
    {
        var result = new List<string>();

        foreach (var argument in arguments)
        {
            if (argument.Rules != null && !CheckClientRules(argument.Rules, info))
            {
                continue;
            }

            foreach (var value in argument.Value)
            {
                result.Add(ClientArgument.Interpolate(value, answers));
            }
        }

        return result;
    }

    private static bool CheckClientRules(IList<ClientRuleset> rules, LaunchInfo info)
    {
        var success = true;

        foreach (var rule in rules)
        {
            if (rule.Features != null)
            {
                if (rule.Features.TryGetValue("is_demo_mode", out var demo)
                    && demo != info.IsDemoMode)
                {
                    continue;
                }

                if (rule.Features.TryGetValue("has_custom_resolution", out var customReso)
                    && customReso != info.HasCustomResolution)
                {
                    continue;
                }
            }

            success = rule.Action == "allow";
        }

        return success;
    }

    private static bool CheckRuntimeRules(IList<RuntimeRuleset> rules)
    {
        var success = false;

        foreach (var rule in rules)
        {
            if (!rule.OS.Verify())
            {
                continue;
            }

            success = rule.Action == RuleAction.Allow;
        }

        return success;
    }
}
