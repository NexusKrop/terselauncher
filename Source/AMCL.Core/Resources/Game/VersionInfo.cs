﻿namespace AMCL.Core.Resources.Game;
public record struct VersionInfo
{
    public VersionInfo(string id, string type, string mainClass)
    {
        Id = id;
        Type = type;
        MainClass = mainClass;
    }

    public string Id { get; set; }
    public string Type { get; set; }
    public string MainClass { get; set; }
}
