﻿namespace AMCL.Core.Resources.Game;

using DaanV2.UUID;
using AMCL.Core.Resources.Game.Client;

public record LaunchInfo
{
    public LaunchInfo(ClientMetadata metadata,
        string userName,
        string uuid,
        string logArgument,
        IList<string> classpath,
        string client,
        bool demo = false,
        bool hasCustomResolution = false,
        string accessToken = "",
        string userType = "")
    {
        ClientMetadata = metadata;
        UserName = userName;
        Uuid = uuid;
        LogArgument = logArgument;
        Classpath = classpath;
        UserType = userType;
        AccessToken = accessToken;
        IsDemoMode = demo;
        HasCustomResolution = hasCustomResolution;
        ClientExecutable = client;
    }

    public ClientMetadata ClientMetadata { get; set; }
    public string UserName { get; set; }
    public string Uuid { get; set; }
    public string LogArgument { get; set; }
    public bool IsDemoMode { get; set; }
    public string AccessToken { get; set; }
    public string UserType { get; set; }
    public bool HasCustomResolution { get; set; }
    public IList<string> Classpath { get; set; }
    public string ClientExecutable { get; set; }
}
