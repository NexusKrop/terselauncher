﻿namespace AMCL.Core.Resources.Game;

using Newtonsoft.Json;

public record struct DownloadPointer
{
    [JsonProperty("sha1")]
    public string Hash { get; set; }
    public int Size { get; set; }
    public string Url { get; set; }
}
