﻿namespace AMCL.Core.Resources.Assets;

using Newtonsoft.Json;

public record struct AssetsIndexPointer
{
    public string Id { get; set; }
    [JsonProperty("sha1")]
    public string Hash { get; set; }
    [JsonProperty("size")]
    public int IndexSize { get; set; }
    public int TotalSize { get; set; }
    public string Url { get; set; }
}
