﻿namespace AMCL.Core.Resources.Assets;

using System.Collections.Generic;

public class AssetsIndex
{
    public Dictionary<string, AssetFilePointer> Objects { get; set; } = new();
}
