﻿namespace AMCL.Core.Resources.Assets;

using Newtonsoft.Json;

public record struct AssetFilePointer
{
    [JsonProperty("hash")]
    public string Hash { get; set; }
    [JsonProperty("size")]
    public int Size { get; set; }
}
