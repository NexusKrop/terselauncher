﻿namespace AMCL.Core.Resources.Libraries;
using System.Collections.Generic;

public class Library
{
    public Library(string name)
    {
        Name = name;
    }

    public LibraryPointer? Downloads { get; set; }
    public LibraryExtraction? Extraction { get; set; }
    public IList<LibraryRule>? Rules { get; set; }
    public IDictionary<string, string>? Natives { get; set; }
    public string Name { get; set; }
}
