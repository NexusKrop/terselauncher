﻿namespace AMCL.Core.Resources.Libraries;
using System.Collections.Generic;
using AMCL.Core.Resources.Game;

public record LibraryPointer
{
    public ArtifactPointer? Artifact { get; set; }

    public IDictionary<string, ArtifactPointer>? Classifiers { get; set; }
}
