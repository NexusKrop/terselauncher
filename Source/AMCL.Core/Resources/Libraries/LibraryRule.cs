﻿namespace AMCL.Core.Resources.Libraries;

using Newtonsoft.Json;
using AMCL.Core.Resources;

public class LibraryRule
{
    public string Action { get; set; } = "allow";

    [JsonProperty("os")]
    public OperatingSystemRule? OperatingSystem { get; set; }
}
