﻿namespace AMCL.Core.Resources.Libraries;
using System.Collections.Generic;

public record LibraryExtraction
{
    public LibraryExtraction(ICollection<string> exclude)
    {
        Exclude = exclude;
    }

    public ICollection<string> Exclude { get; }
}
