﻿namespace AMCL.Core.Util;
using System;
using System.Text.Json;

public static class JsonElementExtensions
{
    public static string EnsureGetString(this JsonElement element)
    {
        return element.GetString() ?? throw new InvalidOperationException("Not string or is literal null");
    }
}
