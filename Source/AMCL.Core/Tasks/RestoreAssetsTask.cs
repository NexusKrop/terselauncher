﻿namespace AMCL.Core.Tasks;

using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text.Json;
using System.Threading.Tasks;
using AMCL.Core.Resources.Assets;
using AMCL.Core.Launch;
using AMCL.Core.Network;
using NexusKrop.IceCube;

public class RestoreAssetsTask : ITask
{
    private readonly AssetsIndexPointer _index;

    public RestoreAssetsTask(AssetsIndexPointer index, GameStorageConfig storageConfig)
    {
        GameRepository = storageConfig.Repository;
        Mirror = storageConfig.Mirror;

        _index = index;
        AssetIndexFile = Path.Combine(GameRepository.AssetsIndexesPath, $"{_index.Id}.json");
        ObjectPath = Path.Combine(GameRepository.AssetsObjectsPath, Path.GetFileNameWithoutExtension(AssetIndexFile));
    }

    public IMirrorProvider Mirror { get; }
    public string AssetIndexFile { get; }
    public string ObjectPath { get; }

    public GameRepository GameRepository { get; }

    public string Id => "restoreAssets";

    public async Task Execute(TaskPerformer performer)
    {
        Directory.CreateDirectory(ObjectPath);

        performer.Output("Assets index hash: {0}", _index.Hash);

        if (!await AssetIndexValid())
        {
            await DownloadAssetsIndex(performer);
        }

        await VerifyAssets(performer);
    }

    private async Task VerifyAssets(TaskPerformer performer)
    {
        performer.Output("Parsing assets index...");

        AssetsIndex? index;

        using (var stream = Launcher.OpenReadJson(AssetIndexFile))
        {
            index = await Task.Run(() => Launcher.Serializer.Deserialize<AssetsIndex>(stream));

            Debug.Assert(index != null);
        }

        performer.Output("Done.");

        performer.Output("Restoring assets");

        var total = index.Objects.Count;
        var count = 0;
        var downloadCount = 0;
        var watch = Stopwatch.StartNew();

        foreach (var obj in index.Objects)
        {
            count++;
            var actualPath = Path.Combine(GameRepository.AssetsObjectsPath, obj.Value.Hash[0..2], obj.Value.Hash);

            performer.StatusUpdate("Restoring assets file {0} ({1} of {2})", obj.Key, count, total);

            if (!await PeekFile(obj.Value, actualPath))
            {
                await RestoreFile(obj.Value, actualPath);
                downloadCount++;
            }

            performer.ReportProgress(MathUtil.CalculatePercentage(count, total));
        }

        watch.Stop();

        performer.Output("Assets restored in {0} ({1} files downloaded)", watch.Elapsed, downloadCount);
    }

    private async Task RestoreFile(AssetFilePointer value, string actualPath)
    {
        var hashDir = value.Hash[0..2];

        Directory.CreateDirectory(Path.Combine(GameRepository.AssetsObjectsPath, hashDir));

        await Launcher.DownloadFileAsync($"{Mirror.AssetsRoot}{hashDir}/{value.Hash}",
            actualPath,
            value.Size,
            false);
    }

    private static async Task<bool> PeekFile(AssetFilePointer pointer, string actualPath)
    {
        if (!File.Exists(actualPath))
        {
            return false;
        }


        var actualHash = await Launcher.ByteArrayToStringAsync(SHA1.HashData(await File.ReadAllBytesAsync(actualPath)));
        if (!pointer.Hash.Equals(actualHash, StringComparison.OrdinalIgnoreCase))
        {
            return false;
        }

        return true;
    }

    private async Task DownloadAssetsIndex(TaskPerformer performer)
    {
        Console.WriteLine("Restoring assets index");

        await Launcher.DownloadFileAsync(_index.Url, AssetIndexFile, onProgress: performer.ReportProgress);
    }

    private async Task<bool> AssetIndexValid()
    {
        if (!File.Exists(AssetIndexFile))
        {
            return false;
        }

        var hash = (await Launcher.ByteArrayToStringAsync(SHA1.HashData(File.ReadAllBytes(AssetIndexFile)))).Replace("-", string.Empty);

        Console.WriteLine(hash);

        return hash.Equals(_index.Hash, StringComparison.OrdinalIgnoreCase);
    }
}
