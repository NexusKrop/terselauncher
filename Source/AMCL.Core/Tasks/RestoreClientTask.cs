﻿namespace AMCL.Core.Tasks;
using System.Threading.Tasks;
using AMCL.Core.Resources.Game;
using AMCL.Core.Launch;
using AMCL.Core.Network;

public class RestoreClientTask : ITask
{
    public RestoreClientTask(string name, DownloadPointer client, GameStorageConfig storage)
    {
        _name = name;
        _client = client;
        Mirror = storage.Mirror;
        GameRepository = storage.Repository;
    }

    private readonly string _name;
    private readonly DownloadPointer _client;

    public string Id => "restoreClient";

    public string? JarFile { get; private set; }

    public IMirrorProvider Mirror { get; private set; }
    public GameRepository GameRepository { get; set; }

    public string? ReplacePath { get; set; }

    public async Task Execute(TaskPerformer performer)
    {
        var actualPath = Path.Combine(GameRepository.VersionPath, _name);
        var jarPath = Path.Combine(actualPath, $"{_name}.jar");

        Directory.CreateDirectory(actualPath);

        var url = _client.Url;

        url = url.Replace("https://launcher.mojang.com/", Mirror.ClientRoot);

        if (!await Launcher.IsFileIntact(jarPath, _client.Hash))
        {
            await Launcher.DownloadFileAsync(url, jarPath, _client.Size, true, performer.ReportProgress);
        }

        JarFile = jarPath;
    }
}
