﻿namespace AMCL.Core.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class TaskPerformer
{
    private readonly Action<string> _msg;
    private readonly Action<int>? _progress;
    private readonly Action<string> _status;

    public TaskPerformer(Action<string> msg, Action<int>? progress = null, Action<string> status = null)
    {
        _msg = msg;
        _progress = progress;
        _status = status;
    }

    public void ReportProgress(int progress)
    {
        _progress?.Invoke(progress);
    }

    public void StatusUpdate(string? message)
    {
        if (message == null)
        {
            return;
        }

        _status.Invoke(message);
    }

    public void StatusUpdate(string? message, params object[] args)
    {
        if (message == null)
        {
            return;
        }

        _status.Invoke(string.Format(message, args));
    }

    public void Output(string? message)
    {
        if (message == null)
        {
            return;
        }

        _msg.Invoke(message);
    }

    public void Output(string? message, params object[] args)
    {
        if (message == null)
        {
            return;
        }

        _msg.Invoke(string.Format(message, args));
    }
}
