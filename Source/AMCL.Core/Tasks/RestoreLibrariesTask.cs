﻿namespace AMCL.Core.Tasks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using AMCL.Core.Resources.Libraries;
using AMCL.Core.Tasks;
using AMCL.Core.Launch;
using AMCL.Core.Network;

public class RestoreLibrariesTask : ITask
{
    public RestoreLibrariesTask(IList<Library> libraries, string nativesFolder, GameStorageConfig storageConfig)
    {
        _libraries = libraries;
        NativesFolder = nativesFolder;
        Mirror = storageConfig.Mirror;
        GameRepository = storageConfig.Repository;
    }

    private readonly IList<Library> _libraries;
    private readonly List<string> _libPaths = new();

    public string Id => "restoreLibraries";

    public string NativesFolder { get; }

    public IMirrorProvider Mirror { get; }
    public GameRepository GameRepository { get; }

    public IList<string> Libraries => _libPaths;

    public async Task Execute(TaskPerformer performer)
    {
        performer.Output("Restoring libraries");
        string? libraryFile = null;

        var metaInf = Path.Combine(NativesFolder, "META-INF");

        if (Directory.Exists(NativesFolder))
        {
            Directory.Delete(NativesFolder, true);
        }
        Directory.CreateDirectory(NativesFolder);

        foreach (var library in _libraries)
        {
            performer.ReportProgress(-1);

            if (library.Rules != null && !await CheckRules(library))
            {
                continue;
            }

            if (library.Downloads != null)
            {
                libraryFile = await RestoreLibrary(library.Downloads, performer);
            }

            if (libraryFile != null)
            {
                _libPaths.Add(libraryFile);
            }

            if (library.Natives != null)
            {
                performer.Output("Extract natives for library {0}", library.Name);
                await ExtractNatives(library, NativesFolder);
            }
        }

        if (Directory.Exists(metaInf))
        {
            Directory.Delete(metaInf, true);
        }
    }

    private async Task ExtractNatives(Library library, string baseFolder)
    {
        // Natives object is a dictionary of platform identifier to native jar pointers.
        // When we restore natives, do this:
        //   - Enumerate the natives dictionary, and perform OperatingSystem.IsOsPlatform on the key. If false, skip to next.
        //     If true, extract the natives.
        //   - Complete when there are no more natives to evaluate.

        // To extract the natives, simply restore the JAR and extract it to the baseFolder.

        // Sanity checks
        Debug.Assert(library.Natives != null);
        Debug.Assert(library.Downloads != null);
        Debug.Assert(library.Downloads.Classifiers != null);

        var subjects = new List<string>(3);

        foreach (var native in library.Natives)
        {
            if (!OperatingSystem.IsOSPlatform(native.Key))
            {
                continue;
            }

            subjects.Add(native.Value);
        }

        foreach (var subject in subjects)
        {
            var native = library.Downloads.Classifiers[subject];

            Debug.Assert(native != null);

            var path = await Launcher.RestoreArtifact(GameRepository.LibrariesPath, native);

            Debug.Assert(File.Exists(path));

            using var stream = File.OpenRead(path);
            var archive = new ZipArchive(stream);
            await Task.Run(() =>
            {
                foreach (var entry in archive.Entries)
                {
                    if (entry == null)
                    {
                        continue;
                    }

                    var target = Path.Combine(baseFolder, entry.FullName);

                    if (Path.EndsInDirectorySeparator(target))
                    {
                        // This is a directory
                        continue;
                    }

                    if (IsNativeExcluded(library.Extraction, entry.FullName))
                    {
                        continue;
                    }

                    entry.ExtractToFile(target, true);
                }
            });
        }
    }

    private static bool IsNativeExcluded(LibraryExtraction? extraction, string entryName)
    {
        if (entryName.StartsWith("META-INF"))
        {
            // No native library is in META-INF
            return true;
        }

        return false;
    }


    private async Task<string?> RestoreLibrary(LibraryPointer downloads, TaskPerformer performer)
    {
        if (downloads.Artifact != null)
        {
            performer.Output("Downloading library {0}", downloads.Artifact.Path);
            downloads.Artifact.Url = downloads.Artifact.Url.Replace("https://libraries.minecraft.net/", Mirror.LibrariesRoot);

            return await Launcher.RestoreArtifact(GameRepository.LibrariesPath, downloads.Artifact, performer.ReportProgress);
        }

        return null;
    }

    private static async Task<bool> CheckRules(Library library)
    {
        // Without a passing {action: "allow"}, and rules exist, the action is disallow by default
        // Only these cases will cause action allow:
        //   1. If the rules does not exist.
        //   2. All rules present action: allow.
        var allow = false;

        if (library.Rules == null || library.Rules.Count == 0)
        {
            return true;
        }

        await Task.Run(() =>
        {
            foreach (var rule in library.Rules!)
            {
                if (rule.OperatingSystem != null && !rule.OperatingSystem.Verify())
                {
                    continue;
                }

                if (rule.Action == "disallow")
                {
                    allow = false;
                    continue;
                }

                if (rule.Action == "allow")
                {
                    allow = true;
                    continue;
                }

                allow = true;
            }
        });

        return allow;
    }
}
