﻿namespace AMCL.Core.Tasks;
using System.Threading.Tasks;

public interface IResultingTask<T>
{
    string Id { get; }
    Task<T> Execute(TaskPerformer performer);
}
