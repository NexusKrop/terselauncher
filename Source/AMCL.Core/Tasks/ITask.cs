﻿namespace AMCL.Core.Tasks;
using System.Threading.Tasks;

public interface ITask
{
    string Id { get; }
    Task Execute(TaskPerformer performer);
}
