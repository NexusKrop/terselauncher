﻿namespace AMCL.Core.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Provides the official Mojang source for restoring game properties.
/// </summary>
public sealed class MojangMirrorProvider : IMirrorProvider
{
    public string AssetsRoot => "https://resources.download.minecraft.net/";

    public string LibrariesRoot => "https://libraries.minecraft.net/";

    public string ClientRoot => "https://launcher.mojang.com/";

    public string VersionManifest => "https://piston-meta.mojang.com/mc/game/version_manifest_v2.json";
}
