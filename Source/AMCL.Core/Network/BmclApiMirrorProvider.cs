﻿namespace AMCL.Core.Network;

public class BmclApiMirrorProvider : IMirrorProvider
{
    public string AssetsRoot => "https://bmclapi2.bangbang93.com/assets/";

    public string LibrariesRoot => "https://bmclapi2.bangbang93.com/maven/";

    public string ClientRoot => "https://bmclapi2.bangbang93.com/";

    public string VersionManifest => "https://bmclapi2.bangbang93.com/mc/game/version_manifest_v2.json";
}
