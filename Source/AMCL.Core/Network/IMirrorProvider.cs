﻿namespace AMCL.Core.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface IMirrorProvider
{
    string AssetsRoot { get; }
    string LibrariesRoot { get; }
    string ClientRoot { get; }
    string VersionManifest { get; }
}
