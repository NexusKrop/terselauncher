﻿namespace AMCL.Core.Launch;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NexusKrop.IceCube;
using System;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AMCL.Core.Resources.Game;
using AMCL.Core.Tasks;

public static class Launcher
{
    public static readonly TaskPerformer ConsolePerformer = new(Console.WriteLine);

    public const string Name = "AMCL";

    public static readonly string Version = Assembly.GetExecutingAssembly().GetName().Version?.ToString()
        ?? "unknown";

    public static readonly SHA1 SHA1 = SHA1.Create();
    public static readonly HttpClient HttpClient = new();

    public static readonly NamingStrategy NamingStrategy = new CamelCaseNamingStrategy();

    public static readonly IContractResolver ContractResolver = new DefaultContractResolver
    {
        NamingStrategy = NamingStrategy,
    };

    public static readonly JsonSerializerSettings SerializerOptions = new()
    {
        ContractResolver = ContractResolver
    };

    public static readonly JsonSerializer Serializer = new()
    {
        ContractResolver = ContractResolver
    };

    public const string ClasspathPlaceholder = "..__XX_CLASS_PATH__..";

    public static JsonWriter CreateJson(string file)
    {
        return new JsonTextWriter(new StreamWriter(File.Create(file)));
    }

    public static JsonReader OpenReadJson(string file)
    {
        return new JsonTextReader(new StreamReader(File.OpenRead(file)));
    }

    public static async Task<string> RestoreArtifact(string artifactRoot, ArtifactPointer pointer,
        Action<int>? onProgress = null, Action? onComplete = null)
    {
        var actualPath = Path.Combine(artifactRoot, pointer.Path.Replace('/', Path.DirectorySeparatorChar));

        Directory.CreateDirectory(Path.GetDirectoryName(actualPath)!);

        if (!await IsFileIntact(actualPath, pointer.Hash))
        {
            await DownloadFileAsync(pointer.Url, actualPath, pointer.Size, true, onProgress, onComplete);
        }

        return actualPath;
    }

    public static string GetNativeClassifier()
    {
        if (OperatingSystem.IsWindows())
        {
            return "natives-windows";
        }

        if (OperatingSystem.IsLinux())
        {
            return "natives-linux";
        }

        if (OperatingSystem.IsMacOS())
        {
            return "natives-macos";
        }

        throw new PlatformNotSupportedException("Natives classifier is unavailable on this platform!");
    }

    public static Task<string> ByteArrayToStringAsync(byte[] arrInput)
    {
        var sOutput = new StringBuilder(arrInput.Length);

        try
        {
            int i;
            for (i = 0; i < arrInput.Length; i++)
            {
                sOutput.Append(arrInput[i].ToString("X2"));
            }
        }
        catch (Exception e)
        {
            return Task.FromException<string>(e);
        }

        return Task.FromResult(sOutput.ToString());
    }

    public static async Task ExecuteTask(ITask task)
    {
        Console.WriteLine(":{0}", task.Id);
        await task.Execute(ConsolePerformer);
    }

    public static async Task<T> ExecuteTask<T>(IResultingTask<T> task)
    {
        Console.WriteLine(":{0}", task.Id);
        return await task.Execute(ConsolePerformer);
    }

    public static async Task<bool> IsFileIntact(string file, string hash)
    {
        if (!File.Exists(file))
        {
            return false;
        }

        var get = await ByteArrayToStringAsync(SHA1.HashData(await File.ReadAllBytesAsync(file)));

        return get.Equals(hash, StringComparison.OrdinalIgnoreCase);
    }

    public static async Task DownloadFileAsync(string url, string destination, int estimatedSize = 0, bool successMsg = false,
        Action<int>? onProgress = null, Action? onComplete = null)
    {
        using var local = File.Create(destination);
        using var remote = await HttpClient.GetStreamAsync(url);

        if (estimatedSize > 0)
        {
            var mx = estimatedSize / 100;
            var counter = 0;
            var swab = 0;

            var bw = new BinaryWriter(local);
            var br = new BinaryReader(remote);

            var estimatedStr = estimatedSize.ToString();
            var fileName = Path.GetFileName(destination);

            try
            {
                while (remote.CanRead && counter < estimatedSize && local.CanWrite)
                {
                    counter++;
                    swab++;
                    bw.Write(br.ReadByte());

                    if (swab >= mx)
                    {
                        // Report progress
                        swab = 0;

                        if (onProgress != null)
                        {
                            onProgress.Invoke(MathUtil.CalculatePercentage(counter, estimatedSize));
                        }
                        else
                        {
                            // Report progress via console (stdout)
                            Console.Write("{2}: {0}/{1} bytes downloaded                   \r",
                                counter.ToString(),
                                estimatedStr,
                                fileName);
                        }
                    }
                }
            }
            catch (EndOfStreamException)
            {
                // discontinue
            }
        }
        else
        {
            if (onProgress != null)
            {
                // Return negative one to instruct "marquee" progress
                onProgress.Invoke(-1);
            }
            else
            {
                Console.Write("Downloading {0}\r", Path.GetFileName(destination));
            }
            await remote.CopyToAsync(local);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
        else if (successMsg)
        {
            Console.WriteLine("Downloaded {0}                                      ", Path.GetFileName(destination));
        }
    }
}