﻿namespace AMCL.Core.Launch;
using AMCL.Core.Network;

public class GameStorageConfig
{
    public GameStorageConfig(GameRepository repository, IMirrorProvider mirror)
    {
        Repository = repository;
        Mirror = mirror;
    }

    public GameRepository Repository { get; }

    public IMirrorProvider Mirror { get; }
}
