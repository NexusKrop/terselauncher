﻿namespace AMCL.Core.Launch;
using System;

public class GameRepository
{
    private GameRepository() : this(Environment.ProcessPath == null ? Environment.CurrentDirectory : Path.GetDirectoryName(Environment.ProcessPath)!)
    {
    }

    public GameRepository(string basePath)
    {
        ArgumentException.ThrowIfNullOrEmpty(basePath);
        BasePath = basePath;

        RepositoryPath = Path.Combine(BasePath, "repository");
        AssetsPath = Path.Combine(RepositoryPath, "assets");
        AssetsIndexesPath = Path.Combine(RepositoryPath, "assets", "indexes");
        AssetsObjectsPath = Path.Combine(RepositoryPath, "assets", "objects");
        LibrariesPath = Path.Combine(RepositoryPath, "libraries");
        VersionPath = Path.Combine(RepositoryPath, "versions");
        UserPath = Path.Combine(RepositoryPath, "home");
        NativePath = Path.Combine(RepositoryPath, "natives");
    }

    public static readonly GameRepository Default = new();

    public string BasePath { get; }
    public string RepositoryPath { get; }
    public string AssetsPath { get; }
    public string AssetsIndexesPath { get; }
    public string AssetsObjectsPath { get; }
    public string LibrariesPath { get; }
    public string VersionPath { get; }
    public string UserPath { get; }
    public string NativePath { get; }

    public static string GetJsonName(string name)
    {
        return string.Format("{0}.json", name);
    }

    public void CreateAll()
    {
        Directory.CreateDirectory(BasePath);
        Directory.CreateDirectory(RepositoryPath);
        Directory.CreateDirectory(AssetsPath);
        Directory.CreateDirectory(AssetsIndexesPath);
        Directory.CreateDirectory(AssetsObjectsPath);
        Directory.CreateDirectory(LibrariesPath);
        Directory.CreateDirectory(VersionPath);
        Directory.CreateDirectory(UserPath);
        Directory.CreateDirectory(NativePath);
    }

    public string GetNativesPath(string version)
    {
        var result = Path.Combine(NativePath, version);
        Directory.CreateDirectory(result);
        return result;
    }

    public bool DoesVersionExist(string version)
    {
        return File.Exists(Path.Combine(VersionPath, version, string.Format("{0}.json", version)));
    }

    public void PrintAll()
    {
        Console.ForegroundColor = ConsoleColor.White;
        Console.WriteLine("Base: {0}", BasePath);
        Console.WriteLine("Repository: {0}", RepositoryPath);
        Console.WriteLine("Assets: {0}", AssetsPath);
        Console.WriteLine("Assets Indexes: {0}", AssetsIndexesPath);
        Console.WriteLine("Assets Objects: {0}", AssetsObjectsPath);
        Console.WriteLine("Libraries: {0}", LibrariesPath);
        Console.WriteLine("Versions: {0}", VersionPath);
        Console.WriteLine("User: {0}", UserPath);
        Console.WriteLine("Natives: {0}", NativePath);
        Console.ResetColor();
    }
}
