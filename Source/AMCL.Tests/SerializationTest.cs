namespace AMCL.Tests;

using Newtonsoft.Json;
using AMCL.Core.Resources.Game.Client;

public class SerializationTest
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void ClientArgument_JsonTest()
    {
        var json = @"[
      ""--username"",
      ""${auth_player_name}"",
      ""--version"",
      ""${version_name}"",
      ""--gameDir"",
      ""${game_directory}"",
      ""--assetsDir"",
      ""${assets_root}"",
      ""--assetIndex"",
      ""${assets_index_name}"",
      ""--uuid"",
      ""${auth_uuid}"",
      ""--accessToken"",
      ""${auth_access_token}"",
      ""--userType"",
      ""${user_type}"",
      ""--versionType"",
      ""${version_type}"",
      {
        ""rules"": [
          {
            ""action"": ""allow"",
            ""features"": {
              ""is_demo_user"": true
            }
          }
        ],
        ""value"": ""--demo""
      },
      {
        ""rules"": [
          {
            ""action"": ""allow"",
            ""features"": {
              ""has_custom_resolution"": true
            }
          }
        ],
        ""value"": [
          ""--width"",
          ""${resolution_width}"",
          ""--height"",
          ""${resolution_height}""
        ]
      }
    ]";

        List<ClientArgument>? code = null;

        Assert.DoesNotThrow(() =>
        {
            code = JsonConvert.DeserializeObject<List<ClientArgument>>(json);
        });

        Assert.Multiple(() =>
        {
            Assert.That(code, Is.Not.Null);
            Assert.That(code![0].Value[0], Is.EqualTo("--username"));
        });
    }
}