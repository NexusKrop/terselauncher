# Deserialization-based `Client.json` Parsing

Before commit [314838c4](https://gitlab.com/NexusKrop/terselauncher/-/commit/314838c452f1d30a51ec5f94f71c43c0eb36ec1f), the parsing of the `client.json` relied on the Document Object Model feature of `System.Text.Json` integrated JSON library.

This caused serious maintainence hassle, as supporting a new type of information from `client.json` means you have to manually inspect all JSON nodes and perform deserialization manually, not to mention nullablity hell and potential performance issues.

With the commit [314838c4](https://gitlab.com/NexusKrop/terselauncher/-/commit/314838c452f1d30a51ec5f94f71c43c0eb36ec1f), TerseLauncher parses the `client.json` by using the [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json) library with deserialization.

The operations below are the steps that are executed by the Lab project, not the GUI project.

## Before Deserialization-based Parsing

TerseLauncher evaluates the `client.json` by Document Object Model, and all command-line arguments are generated when parsing `client.json`.

However, not all parts of the command-line arguments are generated during the `parseClient` task. The classpath value, during this stage of generation, is filled with a placeholder, as the libraries file names are not known until the launcher have restored all libraries by executing `restoreLibraries` task, which depends on the `parseClient` task.

When starting the game, the `launch` task will assemble the classpath with the classpath list provided by `restoreLibraries` task and the full path to the client executable file, then replace the classpath value placeholder with real classpath value during argument merging operation (which enumerates all JVM and game arguments).

## After Deserialization-based Parsing

TerseLauncher deserializes the `client.json` with [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json) library. Arguments are no longer generated at this stage (the `parseClient` task); instead, the `launch` task will generate all the arguments, with full classpath inserted immediately during argument generation rather than when merging arguments.
